const router = require("express").Router();
const {UserRepository} = require("../repositories/UserRepository");
const {authentication} = require("../middleware/authentication");
const {jwtToken} = require("../app/jwt");

router.post("/login",
    authentication.login,
    async (req, res, next) => {
        const repository = new UserRepository();
        try {
            const user = await repository.auth({nickname: req.body.nickname, password: req.body.password});
            const token = await jwtToken(user.uuid);
            res.json({
                code: "[auth] success",
                uuid: user.uuid,
                token
            });
        } catch (e) {
            console.log(e, "FINAL ERROR");
            if (e.message.startsWith("[server]")) {
                next("We are working on a solution. This might take a few minutes, be patient");
            } else {
                res.statusCode = 200;
                res.json({
                    code: "[auth] failed",
                    message: "Contraseña o usuario no válido"
                });
            }
        }
    });

router.post("/register",
    authentication.register,
    async (req, res, next) => {
        const repository = new UserRepository();
        try {
            await repository.register({nickname: req.body.nickname, password: req.body.password});
            res.json({
                code: "[sign in] Success",
                message: "User created successfully"
            });
        } catch (e) {
            if (e.message.startsWith("[server]")) {
                next("We are working on a solution. This might take a few minutes, be patient");
            } else {
                res.statusCode = 403;
                res.json({
                    code: "[sign in] failed.",
                    message: "Username registered."
                });
            }
        }
    },
    function (err, req, res, _) {
        if (err) {
            res.statusCode = 401;
            res.json({
                "status": "error",
                "code": 15,
                "message": err
            });
        }
    });

router.all("/register|login", (req, res) => {
    res.statusCode = 404;
    res.json({
        "status": "error",
        "message": "Method not allowed"
    });
});


router.all("*", (req, res) => {
    res.statusCode = 404;
    res.json({
        "status": "error",
        "message": "Path not found"
    });
});

module.exports = router;

