import React, {FormEvent, useContext, useState} from "react";
import {Link, RouteComponentProps} from "react-router-dom";
import UserContext from "../Context/UserContext";
import instance from "../../api/ApiRequest";
import {authTypes} from "../../hooks/auth/authReducer";
import {useFields} from "../../hooks/form/useFields";

interface iLoginPage {
    history: RouteComponentProps["history"];
}

const LoginForm = (props: iLoginPage) => {

    const [errors, setErrors] = useState("");
    const {dispatch} = useContext(UserContext);

    const [fields, handleFieldChange] = useFields({
        nickname: "thewasta",
        password: "passWord12@"
    });

    const handleSubmit = async (event: FormEvent) => {
        event.preventDefault();
        if (fields.nickname.length > 0 || fields.password.length > 0) {
            try {
                await instance.post("auth/login", {
                    nickname: fields.nickname,
                    password: fields.password
                }).then(response => {
                    if (response.data.code === "[auth] failed") {
                        setErrors("Wrong user or password. Try again");
                    } else {
                        dispatch({
                            type: authTypes.login,
                            payload: {
                                ...{user: response.data}
                            }
                        });
                        props.history.replace("/");
                    }
                });
            } catch (e) {
                setErrors("Service unavailable");
            }
        } else {
            setErrors("Please fill form.");
        }
    };
    return (
        <form onSubmit={handleSubmit}>
            <div className="mb-3">
                <label className={"block mb-2 text-gray-500 text-sm font-bold"} htmlFor={"username"}>
                    Username *
                </label>
                <input
                    value={fields.nickname}
                    onChange={handleFieldChange}
                    type="text"
                    className={"shadow border rounded w-full py-2 px-3 text-gray-500"}
                    id={"nickname"}/>
            </div>

            <div className="mb-3">
                <label className={"block mb-2 text-gray-500 text-sm font-bold"} htmlFor={"password"}>
                    Password *
                </label>
                <input
                    autoComplete={"off"}
                    value={fields.password}
                    onChange={handleFieldChange}
                    type={"password"}
                    className={"shadow border rounded w-full py-2 px-3 text-gray-500"}
                    id={"password"}/>
                {errors.length > 0 ? <p className={"mt-2 italic text-xs text-red-500"}>{errors}</p> : ""}
            </div>

            <div className="flex items-center justify-between">
                <button className="bg-blue-400 hover:bg-blue-dark text-white font-bold py-2 px-4 rounded"
                        type="submit">
                    Sign In
                </button>
                <Link
                    to={"/register"}
                    className="inline-block align-baseline font-bold text-sm text-blue hover:text-blue-darker">
                    I need an account!
                </Link>

            </div>
            <div className="flex items-center justify-end mt-3">
                <Link
                    to={"/reset-password"}
                    className="inline-block align-baseline font-bold text-xs text-gray-500 hover:text-blue-darker">
                    Forgot Password?
                </Link>
            </div>
        </form>
    );
};

export default LoginForm;