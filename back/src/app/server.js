const express = require("express");
const {Server: ioServer} = require("socket.io");
const {createServer} = require("http");

const middleware = require("../helpers/middlewareCompose");
const Socket = require("./socketsio");
const auth = require("../routes/auth");
const passwords = require("../routes/passwords");

class Server {

    #port = process.env.PORT || 8000;
    #app = express();
    #server = createServer(this.#app);
    #io = new ioServer(this.#server);

    #middlewares = () => {
        this.#app.use(middleware());
    };

    configSocket() {
        new Socket(this.#io);
    }

    #routes = () => {
        this.#app.use("/auth", auth);
        this.#app.use("/password", passwords);

    };

    #errorsMiddleware = () => {
        this.#app.use((err, req, res, _) => {
            res.statusCode = 500;
            res.json({
                code: "Service Unavailable",
                message: err
            });
        });
    };

    start() {
        this.#middlewares();
        this.configSocket();
        this.#routes();
        this.#errorsMiddleware();
        this.#server.listen(Number(this.#port), () => {
            console.log("Server is ready on http://localhost:8000");
        });
    }
}

module.exports = Server;