import React from "react";
import {Route, Switch} from "react-router-dom";
import {LoginForm, RegistrationForm, ResetPasswordForm} from "../Components/Login";

const LoginPage = () => {
    return (
        <div className="bg-white w-5/6 md:w-7/12 lg:w-1/3 xl:w-1/4 self-center rounded shadow-lg px-8 py-6 mb-4 flex flex-col">
            <Switch>
                <Route exact path={"/register"} component={RegistrationForm}/>
                <Route exact path={"/reset-password"} component={ResetPasswordForm}/>
                <Route exact path={"/login"} component={LoginForm}/>
            </Switch>
        </div>
    );
};

export default LoginPage;
