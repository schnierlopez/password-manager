import {createContext} from "react";

interface iUserContext {
    user: {
        logged: boolean,
        user?: {
            uuid: string
            token: string
        } | null
    };
    dispatch: any;
    online?: any;
}

let initState: iUserContext = {
    user: {
        logged: false
    },
    dispatch: null
};

const UserContext = createContext(initState);

export default UserContext;