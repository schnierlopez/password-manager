const Database = require("../app/database");
const bcrypt = require("bcrypt");
const {salt} = require("../app/config");
const {v4} = require("uuid");


class UserRepository {
    #connection;
    indice = "users";

    constructor() {
        this.#connection = new Database(this.indice);
    }

    async auth(params) {
        try {
            const result = await this.#connection.findOne({nickname: params.nickname});
            if (result.length === 0) {
                return Promise.reject(Error("[db] User not found"));
            }
            const validatePassword = await bcrypt.compare(params.password, result[0]._source.password);
            if (!validatePassword) {
                return Promise.reject(Error("[auth] Password inválida"));
            }
            return result;
        } catch (e) {
            throw e;
        }
    }

    async register(params) {
        try {
            const result = await this.#connection.findOne({nickname: params.nickname});
            if (result.length !== 0) {
                return Promise.reject(Error("[db] User found"));
            }

            const hSalt = await bcrypt.genSalt(salt);
            const hashedPassword = await bcrypt.hash(params.password, hSalt);

            await this.#connection.insertOne({
                uuid: v4(),
                nickname: params.nickname,
                password: hashedPassword
            });

        } catch (e) {
            throw e;
        }

    }

    async createUser(username, password) {

        const user = await this.#connection.getOne({
            bool: {
                must: [
                    {
                        match: {
                            "nickname": username
                        }
                    }
                ]
            }
        });

        if (user) {
            throw "User already exist.";
        }

        try {
            const hSalt = await bcrypt.genSalt(salt);
            const hashedPassword = await bcrypt.hash(password, hSalt);
            await this.#connection.insertOne({
                uuid: v4(),
                nickname: username,
                password: hashedPassword
            });
        } catch (e) {
            console.log(e);
            console.log(e.text);
        }
    }
}

module.exports = {UserRepository};