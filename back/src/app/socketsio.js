const jwt = require("jsonwebtoken");
const {jwtSecret} = require("./config");

class Socketsio {
    allClients = [];

    constructor(io) {
        console.log(this.allClients);
        this.io = io;
        this.io.use(function (socket, next) {
            jwt.verify(socket.handshake.query.token, jwtSecret, (err, decoded) => {
                if (err) {
                    return next(new Error("Failed JWT Authentication"));
                }
                socket.decoded = decoded;
                console.log(socket.decoded);
                next();
            });
        });

        this.socket();
    }

    socket() {
        this.io.on("connection", (socket) => {
            this.allClients.push(socket.id);
            console.log(this.allClients);
            console.log(socket.id);
            console.log("New client");
            socket.on("message", () => {
                console.log("NUEVO MENSAJE");
                console.log(this.allClients);
            });
            socket.on("disconnect", () => {
                var i = this.allClients.indexOf(socket.id);
                this.allClients.splice(i, 1);
                console.log("socket desconectado");
            });
        });
    }
}


module.exports = Socketsio;