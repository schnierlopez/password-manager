import React, {createContext, useContext, useEffect} from "react";
import {Socket} from "socket.io-client";
import {useSocket} from "../../hooks/socket/useSocket";
import UserContext from "./UserContext";

interface iSocketContext {
    socket?: Socket;
    online?: boolean;
}

let initState: iSocketContext = {};

export const SocketContext = createContext(initState);

const SocketProvider = ({children}: any) => {
    const socketHost = process.env.REACT_APP_SOCKET_DOMAIN || "http://localhost:8000";
    const {user} = useContext(UserContext);
    const {socket, online, conectarSocket, desconectarSocket} = useSocket(socketHost);

    useEffect(() => {
        if (user.logged) {
            conectarSocket();
        }
    }, [user, conectarSocket]);

    useEffect(() => {
        if (!user.logged) {
            desconectarSocket();
        }
    }, [user, desconectarSocket]);

    return (
        <SocketContext.Provider value={{socket, online}}>
            {children}
            <div className={"select-none absolute right-0 m-3 bottom-0"}>
                {
                    online
                        ? <span className={"text-green-400 font-semibold"}>ONLINE</span>
                        : <span className={"text-red-400 font-semibold"}>OFFLINE</span>
                }
            </div>
        </SocketContext.Provider>
    );
};

export default SocketProvider;