import React, {FormEvent} from "react";
import {Link} from "react-router-dom";
import {useFields} from "../../hooks/form/useFields";

const RegistrationForm = () => {

    const [fields, handleInputChange] = useFields({
        nickname: "",
        mail: "",
        password: "",
        confirm: ""
    });
    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
    };
    return (
        <form onSubmit={handleSubmit} autoComplete="off">
            <div className="mb-3">
                <label className={"block mb-2 text-gray-500 text-sm font-bold"} htmlFor={"mail"}>
                    Mail *
                </label>
                <input
                    value={fields.mail}
                    onChange={handleInputChange}
                    type={"email"}
                    placeholder={"mymail@mail.com"}
                    className={"shadow border rounded w-full py-2 px-3 text-gray-500"}
                    id={"mail"}/>
            </div>
            <div className="mb-3">
                <label className={"block mb-2 text-gray-500 text-sm font-bold"} htmlFor={"nickname"}>
                    Username *
                </label>
                <input
                    value={fields.nickname}
                    onChange={handleInputChange}
                    type={"text"}
                    className={"shadow border rounded w-full py-2 px-3 text-gray-500"}
                    id={"nickname"}/>
            </div>

            <div className="mb-3">
                <label className={"block mb-2 text-gray-500 text-sm font-bold"} htmlFor={"password"}>
                    Password *
                </label>
                <input
                    autoComplete={"off"}
                    value={fields.password}
                    onChange={handleInputChange}
                    type={"password"}
                    className={"shadow border rounded w-full py-2 px-3 text-gray-500"}
                    id={"password"}/>
            </div>
            <div className="mb-3">
                <label className={"block mb-2 text-gray-500 text-sm font-bold"} htmlFor={"confirm"}>
                    Password *
                </label>
                <input
                    autoComplete={"off"}
                    value={fields.confirm}
                    onChange={handleInputChange}
                    type={"password"}
                    className={"shadow border rounded w-full py-2 px-3 text-gray-500"}
                    id={"confirm"}/>
            </div>
            <div className="flex items-center justify-between">
                <button className="bg-blue-400 hover:bg-blue-dark text-white font-bold py-2 px-4 rounded"
                        type="submit">
                    Register
                </button>
                <Link to={"/login"}
                      className="inline-block align-baseline font-bold text-sm text-blue hover:text-blue-darker">
                    I have account!
                </Link>
            </div>
        </form>
    );
};

export default RegistrationForm;
