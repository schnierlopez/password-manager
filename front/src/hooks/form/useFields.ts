import {ChangeEvent, useState} from "react";

type Fields = {
    [key: string]: string
}

export function useFields(initState: any): [Fields, (event: ChangeEvent<HTMLInputElement>) => any] {
    const [fields, setField] = useState<Fields>(initState);

    return [fields, function (event: ChangeEvent<HTMLInputElement>) {
        setField({
            ...fields,
            [event.target.id]: event.target.value
        });
    }];
}