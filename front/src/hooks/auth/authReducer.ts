interface iAction {
    type: string;
    payload: any;
}

export const authTypes = {
    login: "[auth] login",
    logout: "[auth] logout"
};

export const authReducer = (state: {}, action: iAction) => {
    switch (action.type) {
        case authTypes.login:
            return {
                ...action.payload,
                logged: true
            };
        case authTypes.logout:
            return {
                logged: false
            };
        default:
            return state;
    }
};