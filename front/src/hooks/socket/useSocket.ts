import {io, Socket} from "socket.io-client";
import {useCallback, useContext, useEffect, useState} from "react";
import UserContext from "../../Components/Context/UserContext";
import {authTypes} from "../auth/authReducer";

export const useSocket = (serverPath: string) => {

    const [socket, setSocket] = useState<any | Socket>(null);
    const [online, setOnline] = useState(false);
    const {dispatch} = useContext(UserContext);
    const connectSocket = useCallback(() => {
        const {token} = JSON.parse(localStorage.getItem("user") as string).user;
        const socketTemp = io(serverPath, {
            transports: ["websocket"],
            autoConnect: true,
            forceNew: true,
            query: {
                token: token
            }
        });
        setSocket(socketTemp);
    }, [serverPath]);

    const disconnectSocket = useCallback(() => {
        socket?.disconnect();
    }, [socket]);

    useEffect(() => {
        setOnline(socket?.connected);
    }, [socket]);

    useEffect(() => {
        socket?.on("connect", () => {
            setOnline(true);
        });
    }, [socket]);

    useEffect(() => {
        socket?.on("disconnect", () => {
            dispatch({
                type: authTypes.logout,
                payload: {}
            });
            setOnline(false);
        });

    }, [socket]);

    return {
        socket,
        online,
        conectarSocket: connectSocket,
        desconectarSocket: disconnectSocket
    };
};