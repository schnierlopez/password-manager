import React from "react";
import {Route, Switch} from "react-router-dom";
import PasswordList from "./PasswordList/PasswordList";

const ContentComponent = () => {

    return (
        <div className={"p-4 w-full"}>
            <Switch>
                <Route path={"/view"} component={PasswordList}/>
            </Switch>
        </div>
    );
};

export default ContentComponent;
