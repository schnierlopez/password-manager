import React from "react";
import {Link} from "react-router-dom";
import {useFields} from "../../hooks/form/useFields";

const ResetPasswordForm = () => {
    const [fields, handleFieldChange] = useFields({
        mail: ""
    });
    const handleSubmit = () => {

    };
    return (
        <form onSubmit={handleSubmit}>
            <div className="mb-3">
                <label className={"block mb-2 text-gray-500 text-sm font-bold"} htmlFor={"username"}>
                    Mail *
                </label>
                <input
                    value={fields.mail}
                    onChange={handleFieldChange}
                    type={"text"}
                    className={"shadow border rounded w-full py-2 px-3 text-gray-500"}
                    id={"mail"}/>
                <p className="text-xs mt-2 text-gray-500">
                    If your mail is register. We will send you a mail to reset password.
                </p>
            </div>

            <div className="flex items-center justify-between">
                <button className="bg-blue-400 hover:bg-blue-dark text-white font-bold py-2 px-4 rounded"
                        type="submit">
                    Send Mail
                </button>
                <Link
                    to={"/login"}
                    className="inline-block align-baseline font-bold text-sm text-blue hover:text-blue-darker">
                    I have my password!
                </Link>
            </div>
        </form>
    );
};

export default ResetPasswordForm;
